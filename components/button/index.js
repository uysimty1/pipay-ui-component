import React, {Component} from 'react';
import { TouchableOpacity, Text, Image } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

export default class Button extends Component {

    propTypes: {
        style           : React.PropTypes.object,
        primary         : React.PropTypes.boolean,
        white           : React.PropTypes.boolean,
        black           : React.PropTypes.boolean,
        allowFontScaling: React.PropTypes.boolean
    }

    getInitialStyle() {
        return {
            button:{
                flexDirection   : 'row',
                alignItems      : 'center',
                justifyContent  : 'center',
                backgroundColor : this.props.primary?COLOR.COLOR_PRIMARY:
                                    (this.props.white?COLOR.COLOR_WHITE:
                                    (this.props.black?COLOR.COLOR_BLACK:
                                            COLOR.COLOR_TRANSPARENT)),
                padding         : SCALE.UNIT*SCALE.PADDING_5,
            },
            text: {
                color       : this.props.white?COLOR.COLOR_PRIMARY:COLOR.COLOR_WHITE,
                fontSize    : SCALE.FONT_SIZE_14,
                fontFamily  : 'Futura',
            }
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle().button
        };
        return computeProps(this.props, defaultProps);
    }

    prepareTextProps() {
        var defaultProps = {
            style: this.getInitialStyle().text,
        };
        return computeProps({style:this.props.textStyle}, defaultProps);
    }

    renderChildren() {
        if(typeof this.props.children == 'string') {
			return <Text {...this.prepareTextProps()} allowFontScaling={this.props.allowFontScaling || false}>{this.props.children}</Text>;
		}else if(Array.isArray(this.props.children)) {
			var newChildren = [];
			React.Children.forEach(this.props.children, (child) => {
				if(typeof child == 'string') {
					newChildren.push(<Text key='text' {...this.prepareTextProps()} allowFontScaling={this.props.allowFontScaling || false}>{child}</Text>);
				}else{
					newChildren.push(child);
				}
	        });
			return newChildren;
		}else{
            return this.props.children;
        }
    }
    render() {
        return(
            <TouchableOpacity {...this.prepareRootProps()} >
                {this.renderChildren()}
            </TouchableOpacity>
        );
    }
}
