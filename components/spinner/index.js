import React, {Component} from 'react';
import { View, ActivityIndicator } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

export default class Spinner extends Component {

    propTypes: {
    }

    getInitialStyle() {
        return {
            view:{
                flex            : 1,
                alignItems      : 'center',
                alignSelf       : 'center',
                justifyContent  : 'space-around',
                position        : 'absolute',
                height          : SCALE.DEVICE_HEIGHT,
                width           : SCALE.DEVICE_WIDTH,
                backgroundColor : 'rgba(0,0,0,0.3)',
                zIndex          : 990,
            },
            spinner:{
                alignSelf       : 'center',
                borderRadius    : 5,
            },
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle().view,
        };
        return computeProps(this.props,defaultProps);
    }

    render() {
        return(
            <View {...this.prepareRootProps()} >
                <ActivityIndicator color={COLOR.COLOR_WHITE} style={this.getInitialStyle().spinner}/>
            </View>
        );
    }
}
