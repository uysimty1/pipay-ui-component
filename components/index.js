import Container from './container';
import Header from './header';
import Content from './content';
import Footer from './footer';
import Button from './button';
import Message from './message';
import Spinner from './spinner';
import HyperLink from './hyperlink';

module.exports = {
	Container,
	...Header,
	Content,
	...Footer,
	Button,
	Message,
	Spinner,
	HyperLink,
}
