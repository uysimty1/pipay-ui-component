import React, {Component} from 'react';
import { View, Text } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

export default class Message extends Component {

    propTypes: {
        style             : React.PropTypes.object,
		allowFontScaling  : React.PropTypes.boolean,
        primary           : React.PropTypes.boolean,
    }

    getInitialStyle() {
        return {
            view:{
                flex            : 1,
                alignItems      : 'center',
                alignSelf       : 'center',
                justifyContent  : 'space-around',
                position        : 'absolute',
                bottom          : 0,
                height          : 100*SCALE.RATIO_Y,
                width           : SCALE.DEVICE_WIDTH,
                backgroundColor : this.props.primary?COLOR.COLOR_PRIMARY:COLOR.COLOR_WHITE,
                zIndex          : 999
            },
            text:{
                textAlign   : 'center',
                color       : this.props.primary?COLOR.COLOR_WHITE:COLOR.COLOR_PRIMARY,
                width       : SCALE.DEVICE_WIDTH,
                fontSize    : SCALE.FONT_SIZE_14,
                fontFamily  : 'Futura',
            },
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle().view,
        };
        return computeProps(this.props,defaultProps);

    }
    prepareTextProps() {
        var defaultProps = {
            style: this.getInitialStyle().text,
        };
        return computeProps({style:this.props.textStyle}, defaultProps);

    }
    renderChildren() {
        if(typeof this.props.children == 'string') {
			return <Text {...this.prepareTextProps()} allowFontScaling={this.props.allowFontScaling || false}>{this.props.children}</Text>;
		}else if(Array.isArray(this.props.children)) {
			var newChildren = [];
			React.Children.forEach(this.props.children, (child) => {
				if(typeof child == 'string') {
					newChildren.push(<Text key='text' {...this.prepareTextProps()} allowFontScaling={this.props.allowFontScaling || false}>{child}</Text>);
				}else{
					newChildren.push(child);
				}
	        });
			return newChildren;
		}
    }
    render() {
        return(
            <View {...this.prepareRootProps()} >
                {this.renderChildren()}
            </View>
        );
    }
}
