import React, {Component} from 'react';
import { View } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

export default class Container extends Component {

    propTypes: {
        style : React.PropTypes.object,
    }

    getInitialStyle() {
        return {
            flex: 1,
            flexDirection   : 'column',
            backgroundColor : COLOR.COLOR_TRANSPARENT,
            height          : SCALE.DEVICE_HEIGHT,
            width           : SCALE.DEVICE_WIDTH,
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle()
        };
        return computeProps(this.props, defaultProps);

    }
    renderChildren() {
        return this.props.children;
    }
    render() {
        return(
            <View {...this.prepareRootProps()} >
                {this.renderChildren()}
            </View>
        );
    }
}
