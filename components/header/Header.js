import React, {Component} from 'react';
import { View } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import HeaderTitle from './HeaderTitle';
import HeaderLeft from './HeaderLeft';
import HeaderRight from './HeaderRight';
import _ from 'lodash';

export default class Header extends Component {

    propTypes: {
        style               : React.PropTypes.object,
        allowFontScaling    : React.PropTypes.boolean,
    }

    getInitialStyle() {
        return {
            flexDirection   : 'row',
            backgroundColor : COLOR.COLOR_PRIMARY,
            height          : SCALE.RATIO_Y*(SCALE.PLATFORM_OS == 'ios'?60:50),
            width           : SCALE.DEVICE_WIDTH,
        }
    }

    prepareRootProps() {
        var defaultProps = {
            style: this.getInitialStyle()
        };
        return computeProps(this.props, defaultProps);

    }

    renderChildren() {
        if(typeof this.props.children == 'string') {
			return <HeaderTitle allowFontScaling={this.props.allowFontScaling || false}>{this.props.children}</HeaderTitle>;
		}else if(Array.isArray(this.props.children)) {
			let newChildren = [],
                headerLeft = [],
                headerRight = [],
                headerTitle = [];

            React.Children.forEach(this.props.children, (child,i) => {
				if(typeof child == 'string') {
					headerTitle.push(<HeaderTitle key={i} {...this.prepareTextProps()} allowFontScaling={this.props.allowFontScaling || false}>{child}</HeaderTitle>);
                }else if(typeof child == 'object'){
                    if(child.type == HeaderTitle) {
                        headerTitle.push(React.cloneElement(child,{key:i}))
                    }else if (child.type == HeaderLeft) {
                        child.key = 'left'
                        headerLeft.push(React.cloneElement(child,{key:i}))
                    }else if (child.type == HeaderRight) {
                        child.key = 'right'
                        headerRight.push(React.cloneElement(child,{key:i}))
                    }
                }
	        });
            if(headerTitle.length>0){
                newChildren.push(headerTitle[0]);
            }
			if(headerLeft.length>0 && headerRight.length > 0){
                newChildren.push(<View key='_headeraction' style={{flexDirection:'row'}}>{headerLeft[0]}{headerRight[0]}</View>)
            }else if(headerLeft.length>0){
                newChildren.push(<View key='_headeraction' style={{flexDirection:'row'}}>{headerLeft[0]}<HeaderRight key='_right' /></View>)
            }else{
                newChildren.push(<View key='_headeraction' style={{flexDirection:'row'}}><headerLeft key='_left'/>{headerRight[0]}</View>)
            }
            return newChildren;
		}else{
            return this.props.children;
        }
    }
    render() {
        return(
            <View {...this.prepareRootProps()} >
                {this.renderChildren()}
            </View>
        );
    }
}
