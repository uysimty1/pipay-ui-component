import React, {Component} from 'react';
import { Linking, Alert, Text } from 'react-native';
import computeProps from '../../utils/computeProps';
import SCALE from '../../utils/scale';
import COLOR from '../../utils/color';
import _ from 'lodash';

const urlRegex =/(\b((http(s)?|ftp|file):\/\/.)?(www\.)?([-a-zA-Z0-9@%_\+~#=]{2,256})+(\.[a-z]{2,6})+(?:\/[\+~%\/.\w-_]*)?(\?(?:[-\+=&;%@.\w_]*)?)?#?(?:[\w]*))/ig;
const telRegex =/([0-9+\(]{1}[0-9 +\(\)]{4,}[0-9)]{1})+/ig;

export default class HyperLink extends Component {

    propTypes: {
        style : React.PropTypes.object,
        linkColor : React.PropTypes.string,
        underline : React.PropTypes.boolean,
    }
    prepareRootProps() {
        var defaultProps = {};
        return computeProps(this.props, defaultProps);

    }

    _handleClick(url){
        Linking.canOpenURL(url).then(supported => {
          if (supported) {
            if(url.substring(0,4) == "tel:"){
                // Works on both iOS and Android
                Alert.alert(
                    '',
                    'Carrier charges may apply.\nCalls are placed through your mobile carrier, not over the Internet.',
                    [
                        {text: 'Cancel'},
                        {text: 'Call', onPress: () => Linking.openURL(url)},
                    ]
                );
            }else{
                Linking.openURL(url);
            }
          } else {
            console.log('Don\'t know how to open URI: ' + url);
          }
        });
     };

     _getHyperLinkStyle(){
         return {
             color: this.props.linkColor?this.props.linkColor:'#00E',
             textDecorationLine: (this.props.underline === false)?'none':'underline',
         }
     }
    _linkifyPhone(text,n){
        let resChildren = [], _text = text+"$", a = '',i=0;
        text.replace(telRegex,(tel) => {
            [a,_text] = _text.split(new RegExp(tel.replace(/([+\(\)])/g,(a)=>("\\"+a))+'(.+)'),2);
            if(a)resChildren.push(a.replace(/ \\r\\n /g, "\r\n").replace(/\\\\/g,'\\'));
            resChildren.push(<Text allowFontScaling={false} key={n+"_"+(i++)}style={this._getHyperLinkStyle()} onPress={()=>this._handleClick("tel:"+tel)}>{tel}</Text>);
        });
        _text = _text.substring(-1,_text.length-1);
        if(_text)resChildren.push(_text.replace(/ \\r\\n /g, "\r\n").replace(/\\\\/g,'\\'));
        return resChildren
    }

    _linkify(text,n){
        text = text.replace(/\\/g,'\\\\').replace(/\r?\n/g, " \\r\\n ");
        let resChildren = [], _text = text+"$", a = '',i=0;
        text.replace(urlRegex,(url) => {
            [a,_text] = _text.split(new RegExp(url.replace(/\?/g, "\\?")+'(.+)'),2);
            if(a)resChildren=resChildren.concat(this._linkifyPhone(a,"_ht_"+n+"_"+i));
            let _url = url;
            if(url.toLowerCase().substring(0,4) != "http"){
                _url = "http://"+url;
            }
            resChildren.push(<Text allowFontScaling={false} key={"_ht_"+n+"_"+(i++)}style={this._getHyperLinkStyle()} onPress={()=>this._handleClick(_url)}>{url}</Text>);
        });
        _text = _text.substring(-1,_text.length-1);
        if(_text)resChildren=resChildren.concat(this._linkifyPhone(_text,"_ht_"+n+"_"+i));
        return resChildren
    }

    renderChildren() {
        let n=1;
        if(typeof this.props.children == 'string') {
			return this._linkify(this.props.children,n);
		}else if(Array.isArray(this.props.children)) {
			var newChildren = [];
			React.Children.forEach(this.props.children, (child) => {
                n++;
				if(typeof child == 'string') {
					newChildren.push(this._linkify(child,n));
				}else{
					newChildren.push(child);
				}
	        });
			return newChildren;
		}else{
            return this.props.children;
        }
    }
    render() {
        return(
            <Text allowFontScaling={false} {...this.prepareRootProps()} >
                {this.renderChildren()}
            </Text>
        );
    }
}
