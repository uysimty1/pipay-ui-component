import Components from './components';
import Scale from './utils/scale';
import Color from './utils/color';
module.exports = {
    /**COMPONENTS**/
	...Components,

	/** UTILS **/
	...Scale,
	...Color,
}
