import { Dimensions, Platform } from "react-native";

// Precalculate Device Dimensions for better performance
const x = Dimensions.get('window').width;
const y = Dimensions.get('window').height;

// Calculating ratio from iPhone breakpoints
const ratioX = Platform.OS == 'ios'? (x < 414 ? (x < 375 ? 0.85 : 1) : 1.10):
( x < 320 ? 0.8 : ( x < 360 ? 0.85 : ( x < 375 ? 0.9 : ( x < 480 ? 1 : ( x < 640 ? 1.2 : ( x < 960 ? 1.5 : ( x < 1280 ? 2 : 3 )))))));
const ratioY = Platform.OS == 'ios'? (y < 736 ? (y < 667 ? 0.87 : 1) : 1.10):
( y < 480 ? 0.7 : ( y < 568 ? 0.75 : ( y < 640 ? 0.8 : ( y < 720 ? 0.85 : ( y < 960 ? 1 : ( y < 1440 ? 1.5 : ( y < 1920 ? 2 : 3 )))))));

// We add an em() shortcut function
function em(value) {
  return Math.round(ratioX * value * 100)/100;
}

module.exports = {
	// GENERAL
	DEVICE_WIDTH	: x,
	DEVICE_HEIGHT	: Platform.OS == 'ios'?y:(y-10*ratioY),
	RATIO_X			: ratioX,
	RATIO_Y			: ratioY,
	UNIT			: em(1),
    PLATFORM_OS		: Platform.OS,

	// FONT
	FONT_SIZE_10	: em(10),
	FONT_SIZE_12	: em(12),
	FONT_SIZE_13	: em(13),
	FONT_SIZE_14	: em(14),
	FONT_SIZE_16	: em(16),
	FONT_SIZE_18	: em(18),
	FONT_SIZE_20	: em(20),
	FONT_SIZE_24	: em(24),

    // PADDING
    PADDING_5       : em(5),
    PADDING_8       : em(8),
    PADDING_10      : em(10),
    PADDING_12      : em(12),
    PADDING_15      : em(15),
    PADDING_18      : em(18),
    PADDING_20      : em(20),

    // MARGIN
    MARGIN_5        : em(5),
    MARGIN_8        : em(8),
    MARGIN_10       : em(10),
    MARGIN_12       : em(12),
    MARGIN_15       : em(15),
    MARGIN_18       : em(18),
    MARGIN_20       : em(20),
}
